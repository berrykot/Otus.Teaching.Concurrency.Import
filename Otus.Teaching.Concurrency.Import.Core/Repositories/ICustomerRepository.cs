using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Handler.Repositories
{
    public interface ICustomerRepository
    {
        void Init();
        void AddCustomer(Customer customer);
        Task AddCustomerAsync(Customer customer)
        {
            return Task.Run(() => AddCustomer(customer));
        }
    }
}