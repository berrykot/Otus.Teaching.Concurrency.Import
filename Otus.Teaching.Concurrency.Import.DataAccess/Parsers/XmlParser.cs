﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser
        : IDataParser<List<Customer>>
    {
        private string _dataFilePath;
        private XmlSerializer _xmlFormatter;
        public XmlParser(string xmlPath)
        {
            _dataFilePath = xmlPath;
            _xmlFormatter = new XmlSerializer(typeof(CustomersList));
        }
        public List<Customer> Parse()
        {
            using var stream = new StreamReader(_dataFilePath);
            var data = (CustomersList)_xmlFormatter.Deserialize(stream);
            return data.Customers;
        }
    }
}