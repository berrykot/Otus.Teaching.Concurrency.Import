﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Loader.Tests
{
	[TestClass()]
	public class ProgramTests
	{
		const string xmlPath = @"C:\Users\Kot\source\repos\Otus.Teaching.Concurrency.Import\Otus.Teaching.Concurrency.Import.Loader\bin\Debug\netcoreapp3.1\customers.xml";
		const string exePath = @"C:\Users\Kot\source\repos\Otus.Teaching.Concurrency.Import\GenerateCustomersDataFile\bin\Debug\netcoreapp3.1\GenerateCustomersDataFile.exe";
		
		[TestMethod()]
		[DataRow(null)]//По умолчанию
		[DataRow(new string[] { xmlPath })]
		[DataRow(new string[] { xmlPath, exePath })]
		public void MainTest(string[] args)
		{
			Program.Main(args);
		}
	}
}
