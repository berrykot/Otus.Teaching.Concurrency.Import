﻿using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories.MsSql;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories.Sqlite;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{

	class XmlDataLoader
        : IDataLoader
    {
        private readonly List<Customer> customers;
        private readonly int threadCount;
        private readonly int tryCount;
        private readonly CustomerRepositoryFactory customerRepositoryFactory;
        public bool IsAsync { get; set; }

        public XmlDataLoader(List<Customer> customers, int threadCount, int tryCount, DbType dbType)
        {
            this.customers = customers ?? throw new ArgumentNullException(nameof(customers));
            this.threadCount = threadCount;
            if (threadCount < 1) throw new ArgumentException("threadCount must be greater 0");
            this.tryCount = tryCount;
            if (tryCount < 1) throw new ArgumentException("tryCount must be greater 0");
            this.customerRepositoryFactory = new CustomerRepositoryFactory(dbType);
            InitDb();
        }

        //public void LoadData()
        //{
        //    if (IsAsync)
        //        StartLoadDataAsync().Wait();
        //    else
        //        StartLoadData();
        //}

        //private Task StartLoadDataAsync()
        //{
        //    return Task.Run(StartLoadData);
        //}
        public void LoadData()
        {
            var itemsByThreadCount = customers.Count / threadCount;
            var iLast = threadCount - 1;
            for (var i = 0; i < threadCount; i++)
            {
                var startIndex = i * itemsByThreadCount;
                var lastIndex = i != iLast ? startIndex + itemsByThreadCount : customers.Count - 1;
                var thread = new Thread(_ => ExecuteAndShowEllapsedTime(startIndex, lastIndex).Wait());
                thread.Start();
                thread.Join();
            }
        }

        private void InitDb()
        {
            //Иначе все потоки начнут создавать БД и таблицу одновременно и получим Exception
            using var repository = customerRepositoryFactory.GetRepository();
            repository.Init();
        }

        private async Task ExecuteAndShowEllapsedTime(int startIndex, int lastIndex)
        {
            var (time, notSuccessCount) = await GetLoadDataTimeInThreadAsync(startIndex, lastIndex);
            Console.WriteLine($"Время обработки с {startIndex} по {lastIndex} номер {time} секунд.{Environment.NewLine}Не обработано {notSuccessCount} из {lastIndex - startIndex} клиентов.");
        }

        private async Task<(float, int)> GetLoadDataTimeInThreadAsync(int startIndex, int lastIndex)
        {
            var sw = new Stopwatch();
            sw.Start();
            var notSuccessCount = await LoadDataInThreadAsync(startIndex, lastIndex);
            sw.Stop();
            float elapsedTime = (float)sw.ElapsedMilliseconds / 1000;
            return (elapsedTime, notSuccessCount);
        }

        private async Task<int> LoadDataInThreadAsync(int startIndex, int lastIndex)
        {
            var notSuccessCount = 0;
            var customersByThread = customers.GetRange(startIndex, lastIndex - startIndex);
            using var repository = customerRepositoryFactory.GetRepository();
            foreach (var customer in customersByThread)
                if (!await TryAddCustomerAsync(repository, customer))
                    notSuccessCount++;
            return notSuccessCount;
        }

        private async Task<bool> TryAddCustomerAsync(ICustomerRepository repository, Customer customer)
        {
            for (var i = 0; i < tryCount; i++)
            {
                try
                {
                    if (IsAsync)
                        await repository.AddCustomerAsync(customer);
                    else
                        repository.AddCustomer(customer);
                    return true;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.ToString());
                }
            }
            return false;
        }
    }
}
