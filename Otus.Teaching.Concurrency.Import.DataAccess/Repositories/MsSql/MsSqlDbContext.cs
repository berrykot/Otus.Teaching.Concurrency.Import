﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories.MsSql
{
	public class MsSqlDbContext : CustomersDbContext
    {
        public MsSqlDbContext()
        {
        }

        public override void Init()
        {
            Database.EnsureCreated();
            Database.ExecuteSqlRaw("DELETE FROM Customers");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Server=(localdb)\MSSQLLocalDB;Database=Customers;Trusted_Connection=True;");
            }
        }
    }
}
