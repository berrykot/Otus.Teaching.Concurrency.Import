﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
	public class CustomersDbContext : DbContext
	{
		public DbSet<Customer> Customers { get; set; }
		public virtual void Init()
		{
		}
	}
}
