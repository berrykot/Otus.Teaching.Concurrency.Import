using System;
using System.Threading;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository
        : ICustomerRepository
    {
        private static Random rnd = new Random(Guid.NewGuid().GetHashCode());
        public void AddCustomer(Customer customer)
        {
            var sleepTime = rnd.Next(5, 15);
            Thread.Sleep(sleepTime);
            if (rnd.Next(0, 100) > 90)
                throw new Exception("�� ������� �������� � �����������");
        }

        public void Init()
        {
        }
    }
}