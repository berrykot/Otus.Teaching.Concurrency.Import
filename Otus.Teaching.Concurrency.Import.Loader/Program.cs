﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;

[assembly: InternalsVisibleTo("UnitTestProject1")]
namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.xml");
        private static string _exePath;
        
        internal static void Main(string[] args)
        {
            HandleArgs(args);

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            if(_exePath != null)
                GenerateCustomersDataFileByProcess();
            else
                GenerateCustomersDataFile();

            var data = new XmlParser(_dataFilePath).Parse();
            var loader = new XmlDataLoader(data, 3, 2, DbType.MsSql)
            {
                IsAsync = true
            };

            loader.LoadData();
        }

        private static void HandleArgs(string[] args)
        {
            if (args != null)
            {
                if (args.Length > 0)
                    _dataFilePath = args[0];
                if (args.Length > 1)
                    _exePath = args[1];
            }
        }

        private static void GenerateCustomersDataFileByProcess()
        {
            var proc = new Process()
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = _exePath,
                    Arguments = _dataFilePath
                }
            };
            proc.Start();
        }

        static void GenerateCustomersDataFile()
        {
            var xmlGenerator = new XmlGenerator(_dataFilePath, 3000);
            //xmlGenerator.Generate();
            xmlGenerator.GenerateAsync().Wait();
        }
    }
}