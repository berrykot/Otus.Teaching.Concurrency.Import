﻿using Otus.Teaching.Concurrency.Import.DataAccess.Repositories.MsSql;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories.Sqlite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
	public enum DbType
	{
		Sqlite,
		MsSql
	}
	public class CustomerRepositoryFactory
	{
		public DbType DbType { get; set; }

		public CustomerRepositoryFactory(DbType dbType)
		{
			DbType = dbType;
		}

		public MyCustomerRepository GetRepository()
		{
			return DbType switch
			{
				DbType.MsSql => new MyCustomerRepository(new MsSqlDbContext()),
				DbType.Sqlite => new MyCustomerRepository(new SqliteDbContext()),
				_ => throw new NotImplementedException()
			};
		}
	}
}
