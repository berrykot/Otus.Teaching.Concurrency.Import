﻿using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using System;
using System.Linq;
using System.Runtime.CompilerServices;


namespace GenerateCustomersDataFile
{
	class Program
	{
        private static string _dataFilePath;
        internal static void Main(string[] args)
		{
            HandleArgs(args);
            GenerateCustomersDataFile();
        }

        private static void HandleArgs(string[] args)
        {
            if (args == null || !args.Any())
                throw new ArgumentException("Required command line arguments");

            _dataFilePath = args[0];
        }

        private static void GenerateCustomersDataFile()
        {
            var xmlGenerator = new XmlGenerator(_dataFilePath, 1000);
            xmlGenerator.Generate();
        }
    }
}
